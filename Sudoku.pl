/**********************************************************
***********************************************************
         IA02 P2018 - Programme polog : Sudoku
***********************************************************
**********************************************************/

/**********************************************************
I - REPRESENTATION DES CONNAISSANCES
**********************************************************/

/**********************************************************
1. Représenter une grille vide
**********************************************************/

%permet d'afficher une nouvelle ligne vide
nouvelle_Ligne(Y) :- Y = [0, 0, 0, 0, 0, 0, 0, 0, 0].


%permet d'afficher une grille vide
nouvelle_Grille(L) :- L = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0]
].


/***************************************************************
Prédicats de base utiles
***************************************************************/


imprimer_ligne([X|Q]) :- write(' '), write(X), imprimer_ligne(Q).

imprimer_liste([]):-write(' | ').
imprimer_liste([T|Q]):-write(' | '), write(T), imprimer_liste(Q).


imprimer_grille([]).
imprimer_grille([Y|R]) :- imprimer_liste(Y), nl, imprimer_grille(R).

%pour flatten une liste
flatten([],[]):-!.
flatten([T|Q],Res):-!,flatten(T,Tf),flatten(Q,Qf),concat(Tf,Qf,Res).
flatten(X,[X]).

numeroelement(T,[T|_],1).
numeroelement(R,[_|Q],I):-numeroelement(R,Q,H), I is H+1.

%tester si la ligne contient des zeros ou non
ligne_sans_zero([]).
ligne_sans_zero([T|Q]):-T\=0,ligne_sans_zero(Q).

%concaténer deux listes
concat([],L,L).
concat([X|L1],L2,[X|L3]) :- concat(L1,L2,L3).

/*faire des sous liste avec une liste qui a été flatten*/
invflatten([], _, []):-!.
invflatten(L, Tailleblocs, [DL|ListeConcat]) :-
   length(DL, Tailleblocs),
   append(DL, Listeaconcat, L),
   invflatten(Listeaconcat, Tailleblocs, ListeConcat).


/**********************************************************
2. Représenter une grille partiellement remplie
**********************************************************/

%pour générer une ligne aléatoire
affectationValeur(X) :- random(1, 20, Xpossible), (Xpossible < 10 -> X is Xpossible; X is 0).

genererLigneAleatoire(0, []).  
genererLigneAleatoire(C,Y):-
        C > 0,        
        C1 is C-1,    
        affectationValeur(U),
        Y = [U|T],   
        genererLigneAleatoire(C1, T).

%taper genererLigneAleatoire(9,X). sur la console, une ligne de 9 éléments compris entre 0 et 9 inclus s'affiche


/***************************************************************
TESTER SI LA GRILLE EST VALIDE PAR RAPPORT AUX REGLES DU SUDOKU
***************************************************************/

%bon nombre d'elements par ligne
compter([],X,0).
compter([X|T],X,Y):- compter(T,X,Z), Y is Z+1.
compter([X1|T],X,Z):- X1\=X,compter(T,X,Z).


bon_compte1(L):-compter(L,1,0),!.
bon_compte1(L):-compter(L,1,1),!.

bon_compte2(L):-compter(L,2,0),!.
bon_compte2(L):-compter(L,2,1),!.

bon_compte3(L):-compter(L,3,0),!.
bon_compte3(L):-compter(L,3,1),!.

bon_compte4(L):-compter(L,4,0),!.
bon_compte4(L):-compter(L,4,1),!.

bon_compte5(L):-compter(L,5,0),!.
bon_compte5(L):-compter(L,5,1),!.

bon_compte6(L):-compter(L,6,0),!.
bon_compte6(L):-compter(L,6,1),!.

bon_compte7(L):-compter(L,7,0),!.
bon_compte7(L):-compter(L,7,1),!.

bon_compte8(L):-compter(L,8,0),!.
bon_compte8(L):-compter(L,8,1),!.

bon_compte9(L):-compter(L,9,0),!.
bon_compte9(L):-compter(L,9,1),!.

bon_compte(L):-bon_compte1(L),bon_compte2(L),bon_compte3(L),bon_compte4(L),bon_compte5(L),bon_compte6(L),bon_compte7(L),bon_compte8(L),bon_compte9(L).


/***************************************************************
COMMENT GENERER DES GRILLES VALIDES
***************************************************************/


/*-----------------------------------------------------------------------------------------------------------
Comment générer des lignes de façons aléatoires et qui soient valides
-----------------------------------------------------------------------------------------------------------*/

genererBonneLigneAleatoire(X) :- genererLigneAleatoire(9,X), bon_compte(X); genererBonneLigneAleatoire(X).


/*-----------------------------------------------------------------------------------------------------------
Comment générer une grille de façon aléatoire qui ait des lignes valides
-----------------------------------------------------------------------------------------------------------*/
genererBonneGrilleAleatoireLigne(_, [], 0).
genererBonneGrilleAleatoireLigne(C, [H|T], Nbligne) :-
    Nbligne1 is Nbligne-1,
    
    genererBonneLigneAleatoire(H), genererBonneGrilleAleatoireLigne(C,T, Nbligne1), !. 

/*-----------------------------------------------------------------------------------------------------------
Pour générer une grille aléatoire avec des lignes valides
-----------------------------------------------------------------------------------------------------------*/
bonneGrilleLigne(X) :- genererBonneGrilleAleatoireLigne(9,X,9).

/*-----------------------------------------------------------------------------------------------------------
Pour générer une grille aléatoire avec des ligne set des colonnes valides
-----------------------------------------------------------------------------------------------------------*/
bonneGrilleLigneColonne(C) :- bonneGrilleLigne(C), verificationColonnes(C); bonneGrilleLigneColonne(C).


/*-----------------------------------------------------------------------------------------------------------
pour générer une grille aléatoire respectant totalement les règles du sudoku
-----------------------------------------------------------------------------------------------------------*/
bonneGrilleRandom(C) :- bonneGrilleLigneColonne(C), verificationCases(C); bonneGrilleRandom(C).

/*-----------------------------------------------------------------------------------------------------------
commande console pour afficher de façon lisible une bonne grille générer aléatoirement
-----------------------------------------------------------------------------------------------------------*/
%bonneGrilleRandom(L),imprimer_grille(L).

/*Vérification des colonnes*/

verificationColonnes(L) :- L = [
    [A1, B1, C1, D1, E1, F1, G1, H1, I1],
    [A2, B2, C2, D2, E2, F2, G2, H2, I2],
    [A3, B3, C3, D3, E3, F3, G3, H3, I3],
    [A4, B4, C4, D4, E4, F4, G4, H4, I4],
    [A5, B5, C5, D5, E5, F5, G5, H5, I5],
    [A6, B6, C6, D6, E6, F6, G6, H6, I6],
    [A7, B7, C7, D7, E7, F7, G7, H7, I7],
    [A8, B8, C8, D8, E8, F8, G8, H8, I8],
    [A9, B9, C9, D9, E9, F9, G9, H9, I9]
],

bon_compte([A1, A2, A3, A4, A5, A6, A7, A8, A9]),
    bon_compte([B1, B2, B3, B4, B5, B6, B7, B8, B9]),
    bon_compte([C1, C2, C3, C4, C5, C6, C7, C8, C9]),
    bon_compte([D1, D2, D3, D4, D5, D6, D7, D8, D9]),
    bon_compte([E1, E2, E3, E4, E5, E6, E7, E8, E9]),
    bon_compte([F1, F2, F3, F4, F5, F6, F7, F8, F9]),
    bon_compte([G1, G2, G3, G4, G5, G6, G7, G8, G9]),
    bon_compte([H1, H2, H3, H4, H5, H6, H7, H8, H9]),
    bon_compte([I1, I2, I3, I4, I5, I6, I7, I8, I9]).


/*Vérification des cases*/


verificationCases(L) :- L = [
    [A1, B1, C1, D1, E1, F1, G1, H1, I1],
    [A2, B2, C2, D2, E2, F2, G2, H2, I2],
    [A3, B3, C3, D3, E3, F3, G3, H3, I3],
    [A4, B4, C4, D4, E4, F4, G4, H4, I4],
    [A5, B5, C5, D5, E5, F5, G5, H5, I5],
    [A6, B6, C6, D6, E6, F6, G6, H6, I6],
    [A7, B7, C7, D7, E7, F7, G7, H7, I7],
    [A8, B8, C8, D8, E8, F8, G8, H8, I8],
    [A9, B9, C9, D9, E9, F9, G9, H9, I9]
],

    bon_compte([A1, A2, A3, B1, B2, B3, C1, C2, C3]),
    bon_compte([D1, D2, D3, E1, E2, E3, F1, F2, F3]),
    bon_compte([G1, G2, G3, H1, H2, H3, I1, I2, I3]),
    bon_compte([A4, A5, A6, B4, B5, B6, C4, C5, C6]),
    bon_compte([D4, D5, D6, E4, E5, E6, F4, F5, F6]),
    bon_compte([G4, G5, G6, H4, H5, H6, I4, I5, I6]),
    bon_compte([A7, A8, A9, B7, B8, B9, C7, C8, C9]),
    bon_compte([D7, D8, D9, E7, E8, E9, F7, F8, F9]),
    bon_compte([G7, G8, G9, H7, H8, H9, I7, I8, I9]).

%Un prédicat qui prend en argument une grille et qui est vrai si cette grille respecte les règles imposées du sudoku

  bonneGrille(L) :- L = [
    [A1, B1, C1, D1, E1, F1, G1, H1, I1],
    [A2, B2, C2, D2, E2, F2, G2, H2, I2],
    [A3, B3, C3, D3, E3, F3, G3, H3, I3],
    [A4, B4, C4, D4, E4, F4, G4, H4, I4],
    [A5, B5, C5, D5, E5, F5, G5, H5, I5],
    [A6, B6, C6, D6, E6, F6, G6, H6, I6],
    [A7, B7, C7, D7, E7, F7, G7, H7, I7],
    [A8, B8, C8, D8, E8, F8, G8, H8, I8],
    [A9, B9, C9, D9, E9, F9, G9, H9, I9]
],
%ligne
    bon_compte([A1, B1, C1, D1, E1, F1, G1, H1, I1]),
    bon_compte([A2, B2, C2, D2, E2, F2, G2, H2, I2]),
    bon_compte([A3, B3, C3, D3, E3, F3, G3, H3, I3]),
    bon_compte([A4, B4, C4, D4, E4, F4, G4, H4, I4]),
    bon_compte([A5, B5, C5, D5, E5, F5, G5, H5, I5]),
    bon_compte([A6, B6, C6, D6, E6, F6, G6, H6, I6]),
    bon_compte([A7, B7, C7, D7, E7, F7, G7, H7, I7]),
    bon_compte([A8, B8, C8, D8, E8, F8, G8, H8, I8]),
    bon_compte([A9, B9, C9, D9, E9, F9, G9, H9, I9]),
%colonne
    bon_compte([A1, A2, A3, A4, A5, A6, A7, A8, A9]),
    bon_compte([B1, B2, B3, B4, B5, B6, B7, B8, B9]),
    bon_compte([C1, C2, C3, C4, C5, C6, C7, C8, C9]),
    bon_compte([D1, D2, D3, D4, D5, D6, D7, D8, D9]),
    bon_compte([E1, E2, E3, E4, E5, E6, E7, E8, E9]),
    bon_compte([F1, F2, F3, F4, F5, F6, F7, F8, F9]),
    bon_compte([G1, G2, G3, G4, G5, G6, G7, G8, G9]),
    bon_compte([H1, H2, H3, H4, H5, H6, H7, H8, H9]),
    bon_compte([I1, I2, I3, I4, I5, I6, I7, I8, I9]),

%case
    bon_compte([A1, A2, A3, B1, B2, B3, C1, C2, C3]),
    bon_compte([D1, D2, D3, E1, E2, E3, F1, F2, F3]),
    bon_compte([G1, G2, G3, H1, H2, H3, I1, I2, I3]),
    bon_compte([A4, A5, A6, B4, B5, B6, C4, C5, C6]),
    bon_compte([D4, D5, D6, E4, E5, E6, F4, F5, F6]),
    bon_compte([G4, G5, G6, H4, H5, H6, I4, I5, I6]),
    bon_compte([A7, A8, A9, B7, B8, B9, C7, C8, C9]),
    bon_compte([D7, D8, D9, E7, E8, E9, F7, F8, F9]),
    bon_compte([G7, G8, G9, H7, H8, H9, I7, I8, I9]).


/***************************************************************
PERMETTRE A L'UTILISATEUR DE CHANGER UN ELEMENT D'UNE GRILLE
***************************************************************/
%permet de modifier la valeur de l'element numero C d'une ligne par l'element V
modifier_Ligne([_|Q],1,V,[V|Q]):-!.
modifier_Ligne([T|Q],C,V,[T|R]):-D is C-1, modifier_Ligne(Q,D,V,R).

%si l'utilisateur a modifié la ligne dans les règles du sudoku
modifier_Ligne_ok(L,N,X,Res):-modifier_Ligne(L,N,X,Res),bon_compte(Res).

%permet de modifier la case numero C de la ligne numero L par la valeur V
modifier_Grille([L1|Q],1,C,V,[L2|Q]):-modifier_Ligne(L1,C,V,L2),!.
modifier_Grille([L1|G],L,C,V,[L1|H]):-L2 is L-1, modifier_Grille(G,L2,C,V,H).

%pour modifier une grille et voir si ce qu'on a fait est bon
modifier_Grille_ok(G,L,C,V):-modifier_G(G,L,C,V,Res),bonneGrille(Res),write(Res).


/***************************************************************
COMMENT DONNER LES SOLUTIONS D'UNE GRILLE
***************************************************************/
/*-----------------------------------------------------------------------------------------------------------
Résoudre une ligne 
-----------------------------------------------------------------------------------------------------------*/

resoudre_ligne_boucle(0,[],[]).
resoudre_ligne_boucle(C,L,L):-ligne_sans_zero(L),bon_compte(L).
resoudre_ligne_boucle(C,L,M):-
		(numeroelement(0,L,C))->((
				 modifier_Ligne_ok(L,C,1,Res);
				 modifier_Ligne_ok(L,C,2,Res);
				 modifier_Ligne_ok(L,C,3,Res);
				 modifier_Ligne_ok(L,C,4,Res);
				 modifier_Ligne_ok(L,C,5,Res);
				 modifier_Ligne_ok(L,C,6,Res);
				 modifier_Ligne_ok(L,C,7,Res);
				 modifier_Ligne_ok(L,C,8,Res);
				 modifier_Ligne_ok(L,C,9,Res)	
),C1 is C-1),resoudre_ligne_boucle(C1,Res,M); C2 is C-1,resoudre_ligne_boucle(C2,L,M).

%taper resoudre_ligne_boucle(9,[1,2,0,5,6,0,7,8,0],Res) permet d'afficher la solution

resoudre_grille_boucle([],[]).
resoudre_grille_boucle([T|Q],Res):-resoudre_ligne_boucle(9,T,S),resoudre_grille_boucle(Q,Res1),concat(S,Res1,Res),!.

grille_resolue_ligne_bonne(X,Res,L) :- resoudre_grille_boucle(X,Res), invflatten(Res,9,L).
%resoudre_grille(G):-flatten(G,Res),resoudre_ligne_boucle(9,Res).

afficher_grille_resolue_ligne_bonne(X,Res,L) :- grille_resolue_ligne_bonne(X,Res,L), imprimer_grille(L).

%idee pas encore assez exploitee, manque de temps
%grille_resolue_ligne_colonne_bonne(X,Res,L) :- grille_resolue_ligne_bonne(X,Res,L), verificationColonnes(L).

/***************************************************************
ANNEXE ULTRA UTILE - GRILLE TEST
***************************************************************/
%grille de test
essai([[1, 2, 0, 0, 5, 6, 7, 8, 9],
    [4, 5, 6, 7, 8, 9, 1, 2, 3],
    [7, 0, 9, 1, 2, 3, 4, 5, 6],
    [2, 3, 4, 0, 6, 7, 8, 0, 1],
    [5, 6, 7, 8, 9, 1, 2, 3, 4],
    [8, 0, 1, 2, 0, 4, 5, 6, 7],
    [3, 4, 5, 6, 7, 8, 9, 1, 2],
    [6, 7, 8, 9, 1, 2, 3, 4, 5],
    [9, 1, 2, 3, 0, 5, 6, 7, 0]]).

essai1([[1, 2, 0], 
	[0, 5, 6],
	[7, 8, 9]]).